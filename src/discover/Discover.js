import './css/Discover.css'
import React, { Component } from 'react'
import { Route,Switch,HashRouter,Redirect,NavLink} from 'react-router-dom' //引入路由
import axios from 'axios'

import NewRecord from './NewRecord'
import Ranking from './Ranking'
import Recommend from './Recommend'
import Singer from './Singer'
import SongList from './SongList'
import TheHostStation from './TheHostStation'
import CoverInfo from './recommend/coverInfo/CoverInfo'
export default class Discover extends Component {
  state = {
    bars: [],
    currentTabIndex: 1
  }
  componentDidMount() {
    axios.get('http://localhost:3000/music163').then((response) => {

      this.setState({
        bars: response.data.discoverbars
      })
    }).catch(function (error) {
      console.log(error);
    });

  }

  chooseLink = (barId) => {
    this.setState({
      currentTabIndex: barId
    })
  }

  checkChildrenPage = (params) => {
    switch (params.pagename) {
      case "ranking":
        return <Ranking></Ranking>
      case "songlist":
        return <SongList></SongList>
      case "thehoststation":
        return <TheHostStation></TheHostStation>
      case "singer":
        return <Singer></Singer>
      case "newrecord":
        return <NewRecord></NewRecord>
      case "coverInfo":
        return <CoverInfo></CoverInfo>
      default :
       return <Recommend></Recommend>
    }
  }
  render() {
    const { match } = this.props;
    const navBars = this.state.bars.map((bar) => {
      return (
        <li key={bar.id} onClick={() => this.chooseLink(bar.id)} className={bar.id === this.state.currentTabIndex ? 'active-link' : ''}>
          <NavLink to={'/discover' +bar.path}>{bar.name}</NavLink>
        </li>
      )
    });
    return (
      <div className="Discover">
        <div className='bar-wrap'>
          <div className='bar_bg'>
            <ul>
              {navBars}
            </ul>
            
          </div>
        </div>
        {/* {this.checkChildrenPage(match.params)}  */}
        <HashRouter>
           <Switch>
              <Redirect path="/" exact={true} to="/discover/recommend" />  
              <Redirect path="/discover" exact={true} to="/discover/recommend" />  
              <Route path='/discover/recommend' component={Recommend} />
              <Route path='/discover/ranking' component={Ranking} />
              <Route path='/discover/songlist' component={SongList} />
              <Route path='/discover/thehoststation' component={TheHostStation} />
              <Route path='/discover/singer' component={Singer} />
              <Route path='/discover/newrecord' component={NewRecord} />
              <Route path='/discover/coverInfo/:coverId' component={CoverInfo} />
        </Switch>
        </HashRouter>
       
      </div>
    );
  }
}

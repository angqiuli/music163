
// import ReactAudioPlayer from 'react-audio-player'
import React, { Component, createRef } from 'react'
import { connect } from 'react-redux' //让组件和store连接
import './MyPlayer.scss'
import rootReducer from './reducers/rootReducer';
import { createStore } from 'redux'
const store = createStore(rootReducer)
class Player extends Component {
    state = {
        currentTime: '00:00',
        allTime: '02:52',
        curAllTime: '02:52',
        drag: false,
        song: null,
        playFlag: true,
        clock:false //面板锁定
    }

    playBgImg = require('../public/playbar_8.png')
    myPlayerStyle = {
        width: '100%',
        height: '46px',
        background: 'url(' + this.playBgImg + ') 0px -6px',
        position: 'fixed',
        bottom: '0px'
    }
    auduiPlayerStyle = {
        display: 'none'
    }
    goPreStyle = {
        background: 'url(' + this.playBgImg + ') 0px -130px no-repeat',
        width: '28px',
        height: '28px'
    }
    playingStyle = {
        background: 'url(' + this.playBgImg + ') 0px -164px no-repeat',
        width: '38px',
        height: '38px'
    }
    pauseingStyle = {
        background: 'url(' + this.playBgImg + ') 0px -204px no-repeat',
        width: '38px',
        height: '38px'
    }
    goNextStyle = {
        background: 'url(' + this.playBgImg + ') 0px -130px no-repeat',
        width: '28px',
        height: '28px'
    }
    lineStyle = {
        background: 'url(' + require('../public/statbar.png') + ') no-repeat 0 -28px'
    }
    innerStyle = {
        background: 'url(' + require('../public/statbar.png') + ') no-repeat 0 -64px'
    }
    dotStyle = {
        background: 'url(' + require('../public/iconall.png') + ') no-repeat -42px -252px'
    }
    clockWrapStyle = {
        background:'url(' + this.playBgImg + ') 0px -384px no-repeat'
    }
    clockStyle_unClock={
        background:'url(' + this.playBgImg + ') -80px -399px no-repeat'
    }
    clockStyle_clock = {
        background:'url(' + this.playBgImg + ') -100px -399px no-repeat'
    }
    dot = createRef(null);
    inner = createRef(null);
    line = createRef(null);
    audio = createRef(null);
    playBtnsDom = createRef(null);
    imgDom = createRef(null);
    curTimeDom = createRef(null);
    playWrap = createRef(null);

    startX = 0

    //鼠标按下，开始监听移动
    handleMouseDown = function (e) {
        e.preventDefault(); e.stopPropagation();
        if (!this.startX) {
            this.startX = e.clientX;
        }
        if (this.inner.current.offsetWidth < this.line.current.offsetWidth && this.dot.current.offsetLeft < this.line.current.offsetWidth) {
            this.setState({
                drag: true
            })
            //this.dragFlag = true
            this.inner.current.onmousemove = (e) => {
                e.preventDefault();
                this.handleMouseMove(e)
            }
            this.inner.current.onmouseup = (e) => {
                this.handleMouseUp(e)
            }
            this.inner.current.onmouseout = (e) => {
                this.handleMouseUp(e)
            }
        } else {
            this.handleMouseUp(e);
        }
    }

    doPlay = () => {
        if (this.state.playFlag) {
            this.audio.current.pause();
        } else {
            this.audio.current.play();
            if (this.audio.current.ended) {//播放完了
                //恢复到播放之前状态
                this.reSetPlay()
            }
            //if(!this.interval){
            this.interval = null
            this.interval = setInterval(() => {
                if (this.props.playFlag && this.state.playFlag) {
                    if (this.props.playFlag != this.state.playFlag) {
                        this.setState({
                            playFlag: true
                        })
                    }
                    if (!this.audio.current.ended && this.inner.current.offsetWidth < this.line.current.offsetWidth && this.dot.current.offsetLeft < this.line.current.offsetWidth) {
                        let moveX = 466 / this.changeTimeToMin(this.props.songAllTime) //每500毫秒移动的距离
                        let innerNowWidth = this.inner.current.offsetWidth;
                        let dotNowLeft = this.dot.current.offsetLeft;
                        this.inner.current.style.width = (innerNowWidth + moveX) + 'px'
                        this.dot.current.style.left = (dotNowLeft + moveX) + 'px'
                        this.setCurAllTime()
                        this.setCurTime()
                    } else {
                        //播放完了
                        clearInterval(this.interval)
                        this.reSetPlay()
                    }
                }
            }, 1000)
            //}
        }
        this.setState({
            playFlag: !this.state.playFlag
        })
    }
    reSetPlay = () => {
        console.log("播放完后的curAllTime:" + this.state.curAllTime)
        console.log("播放完后的allTime:" + this.state.allTime)
        this.setState({
            curAllTime: this.state.allTime,
            currentTime: '00:00',
            playFlag: false
        })
        this.inner.current.style.width = '0px'
        this.dot.current.style.left = '0px'
    }
    //人为移动进度条
    handleMouseMove = function (e) {
        console.log("移动")
        console.log(this)
        console.log(this.inner.current.offsetWidth);
        console.log(this.line.current.offsetWidth);
        console.log(this.dot.current.offsetWidth);
        // if(e.pageX < this.playWrap.current.offsetLeft + this.playBtnsDom.current.offsetLeft + this.playBtnsDom.current.offsetWidth +
        //     this.imgDom.current.offsetWidth + 10 + this.curTimeDom.current.offsetWidth + this.dot.current.offsetWidth / 2){
        //         alert(111)
        // }
        if (this.state.drag
            && this.inner.current.offsetWidth < this.line.current.offsetWidth
            && this.dot.current.offsetLeft < this.line.current.offsetWidth) {

            this.startX = !this.startX ? e.pageX : this.startX
            e.preventDefault();
            const moveX = e.pageX - this.startX
            console.log('移动距离：' + moveX)
            // left的值等于开始时候的left加移动的pageX差值
            this.dot.current.style.left = moveX + 'px'
            // // 设置进度条宽度
            this.inner.current.style.width = moveX + 'px'
        } else {
            //播放完
            //重置播放器
            this.reSetPlay()
        }
    }
    handleMouseUp = function (e) {
        console.log('移动取消：' + e.type)
        console.log(e.target)
        this.inner.current.onmousemove = null
        this.inner.current.onmouseup = null
        this.inner.current.onmouseout = null
        this.setState({
            drag: false
        })
        // //总时长
        let allSongTime = this.changeTimeToMin(this.props.songAllTime)
        // //进度条总宽度
        let allWidth = this.line.current.offsetWidth
        let moveX = e.pageX - this.startX
        // //设置音乐播放进度
        let value = (moveX * allSongTime) / allWidth
        this.audio.current.currentTime = value;
        this.setState({
            playFlag: true
        })
        this.audio.current.play()
        this.setCurAllTime()
        this.setCurTime()
    }

    //直接点击选择进度
    handlerClick = function (e) {
        //this.dragFlag = false
        this.setState({
            drag: false
        })
        //总时长
        let allSongTime = this.changeTimeToMin(this.props.songAllTime)
        //进度条总宽度
        let allWidth = this.line.current.offsetWidth

        let startX = this.playWrap.current.offsetLeft + this.playBtnsDom.current.offsetLeft + this.playBtnsDom.current.offsetWidth +
            this.imgDom.current.offsetWidth + 10 + this.curTimeDom.current.offsetWidth + this.dot.current.offsetWidth / 2
        let moveX = e.pageX - startX
        //设置音乐播放进度
        let value = (moveX * allSongTime) / allWidth
        this.dot.current.style.left = parseInt(this.dot.current.offsetLeft) + moveX + 'px'
        // 设置进度条宽度
        this.inner.current.style.width = parseInt(this.inner.current.offsetWidth) + moveX + 'px'
        this.audio.current.currentTime = value;
        this.setState({
            playFlag: true
        })
        this.audio.current.play()
        this.setCurAllTime()
        this.setCurTime()
    }

    setCurAllTime = () => {
        //console.log('当前播放状态：'+this.audio.current.ended +':'+this.audio.current.currentTime)
        let nowAllTime = this.changeTimeToMin(this.state.allTime) - this.audio.current.currentTime
        this.setState({
            curAllTime: this.formateMin(nowAllTime)
        })
    }

    setCurTime = () => {
        this.setState({
            currentTime: this.formateMin(this.audio.current.currentTime)
        })
    }

    //分：秒 换成秒
    changeTimeToMin = (time) => {
        return time ? (parseInt(time.toString().split(':')[0]) * 60 + parseInt(time.toString().split(':')[1])) : 0
    }
    formateMin = (_min) => {
        let sec = parseInt(_min / 60)
        sec = sec >= 10 ? sec : '0' + sec
        _min = parseInt(_min % 60)
        _min = _min >= 10 ? _min : '0' + _min
        return (sec + ':' + _min)
    }
    progressStyle = {
        display: 'inline-block',
        width: '500px',
        height: '10px',
        backgroundColor: 'aquamarine'

    }

    interval = null;
    componentDidMount() {
        this.setState({
            clock:false
        })
        //进度条
        this.interval = setInterval(() => {
            if (!this.state.drag && this.props.playFlag && this.state.playFlag) {
                if (this.props.playFlag != this.state.playFlag) {
                    this.setState({
                        playFlag: true
                    })
                }
                if (!this.audio.current.ended) {
                    let newMoveX = this.audio.current.currentTime * this.line.current.offsetWidth / this.changeTimeToMin(this.props.songAllTime)
                    console.log(newMoveX)
                    this.inner.current.style.width = newMoveX + 'px'
                    this.dot.current.style.left = newMoveX + 'px'
                    this.setCurAllTime()
                    this.setCurTime()
                } else {
                    //播放完了
                    clearInterval(this.interval)
                    //重置播放器
                    this.reSetPlay()
                }
            }
        }, 1000)

    }
    componentWillUnmount() {
        clearInterval(this.interval)
    }

    mouseOutHandler(e){
        //没上锁，并鼠标移出，就隐藏
        if(!this.state.clock && e.type === 'mouseleave') {
            //  e.target.style.display = 'none'
            this.playWrap.current.style.bottom = '-40px'
            this.playWrap.current.style.transition= 'bottom 1.2s'
            this.playWrap.current.style.transitionDelay= '0.5s'
        }        
        //没上锁，并鼠标移进，就显示
        if(!this.state.clock && e.type === 'mouseenter') {
            this.playWrap.current.style.bottom = '0px'                     
            this.playWrap.current.style.transition= 'bottom 1.2s'
            this.playWrap.current.style.transitionDelay= '0s'
       }     
    }
    render() {
        const song = this.props.song;
        if (song) {
            return (
                <div>
                    <div className='my-player' ref={this.playWrap} style={this.myPlayerStyle} onMouseEnter={(e)=>{this.mouseOutHandler(e)}}  onMouseLeave={(e)=>{this.mouseOutHandler(e)}}>
                        <audio ref={this.audio} src={this.props.song} autoPlay={true} controls={true} style={{ display: 'none' }} />
                        <div className='play-wrap' ref={this.playWrap}>
                            <div className='btns' ref={this.playBtnsDom}>
                                <span className='pre' style={this.goPreStyle} onClick={() => { this.goPre() }}></span>
                                <span className='play' style={this.state.playFlag ? this.playingStyle : this.pauseingStyle} onClick={() => { this.doPlay() }}></span>
                                <span className='next' style={this.goNextStyle} onClick={() => { this.goNrxt() }}></span>
                            </div>
                            <img src={this.props.songImg} className='song-img' ref={this.imgDom}></img>
                            <div className='progress-wrap'>
                                <span className='current-time' ref={this.curTimeDom}>{this.state.currentTime}</span>
                                <span className='lineWrap' style={this.lineStyle} onClick={(e) => { this.handlerClick(e) }} ref={this.line}>
                                    <span className='lineInner' ref={this.inner} style={this.innerStyle} >
                                        <span className='lineDot' ref={this.dot} style={this.dotStyle} onMouseDown={(e) => { this.handleMouseDown(e) }} />
                                    </span>
                                </span>
                                <span className='all-time'>{this.state.curAllTime}</span>
                            </div>
                        </div>
                        <div className='clock-wrap' style={this.clockWrapStyle}>
                            <div className='clock' style={!this.state.clock ? this.clockStyle_unClock : this.clockStyle_clock} onClick={()=>this.setState({clock:!this.state.clock})}></div>
                        </div>
                    </div>
                </div>
            )
        } else {
            <div></div>
        }
    }
}
const mapStateToProps = (state) => {
    return {
        song: state.song,
        songAllTime: state.songAllTime,
        songImg: state.songImg,
        playFlag: state.playFlag
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        //控制播放/暂停
        playControler: (playFlag) => {
            dispatch({ type: 'SET_SONG', playFlag: playFlag })
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Player)
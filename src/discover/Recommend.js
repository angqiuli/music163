import React, { Component } from 'react'
import Slider from './recommend/Slider'
import Cover from './recommend/Cover';
import { NavLink,withRouter } from 'react-router-dom'

import headerBGImg from '../../public/index.png'
import './css/Recommend.css'
import axios from 'axios'
class Recommend extends Component {
  
  coverList = [];
  state = {
    covers:this.coverList
  }

  haderStyle = {
    background: 'url(' + headerBGImg + ') -225px -156px no-repeat'
  }

  
  coverList = [];
  componentDidMount(){
    axios.get('http://localhost:3000/music163').then((response) => {
      if(this.coverList.length == 0){
        response.data.hotCovers.map((cover) => {
                  if (cover.id < 9) {
                      this.coverList.push(cover);
                  }
              })
      }
       
   })
  }
  render() {
    return (
      <div className="Recommend">
        <Slider/>
        <div className='rec-content'>
          <div className='content-left'>
            {/* 热门推荐 */}
            <div className='hot-cover'>
              <div className='header' style={this.haderStyle}>
                <a className='header-name'>热门推荐</a>
                <div className='header-tab'>
                  <a>华语</a>|
                  <a>流行</a>|
                  <a>摇滚</a>|
                  <a>民谣</a>|
                  <a>电子</a>
                </div>
                <span className='more'>
                  <NavLink className='more' to='discover/playlist'></NavLink>
                  <span></span>
                </span>
              </div>
              <Cover/>
            </div>            
            {/* 个性化推荐 */}
            <div className='personality-cover'>
              <div className='header' style={this.haderStyle}>
                <a className='header-name'>个性推荐</a>
                <span className='more'>
                  <NavLink className='more' to='discover/playlist'></NavLink>
                  <span></span>
                </span>
              </div>
              <Cover/>
            </div>
            {/* 新碟上架 */}
            {/* 榜单 */}
          </div>
          <div className='content-right'>

          </div>
        </div>
      </div>
    );
  }

}
export default Recommend
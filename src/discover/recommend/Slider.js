import bgImg1 from '../../../public/image/slider/bg/sld-bg1.jpg'
import bgImg2 from '../../../public/image/slider/bg/sld-bg2.jpg'
import bgImg3 from '../../../public/image/slider/bg/sld-bg3.jpg'
import bgImg4 from '../../../public/image/slider/bg/sld-bg4.jpg'
import bgImg5 from '../../../public/image/slider/bg/sld-bg5.jpg'
import bgImg6 from '../../../public/image/slider/bg/sld-bg6.jpg'
import bgContent1 from '../../../public/image/slider/sld-content1.jpg'
import bgContent2 from '../../../public/image/slider/sld-content2.jpg'
import bgContent3 from '../../../public/image/slider/sld-content3.jpg'
import bgContent4 from '../../../public/image/slider/sld-content4.jpg'
import bgContent5 from '../../../public/image/slider/sld-content5.jpg'
import bgContent6 from '../../../public/image/slider/sld-content6.jpg'
import download from '../../../public/image/slider/download.png'
import { Component } from 'react'
import {withRouter}  from 'react-router-dom'
import './Slider.css'

class Slider extends Component {
    sliders = [
        {
            'id': 1,
            'bgImg': bgImg1,
            'contentImg': bgContent1,
            'link': ''
        },
        {
            'id': 2,
            'bgImg': bgImg2,
            'contentImg': bgContent2,
            'link': ''
        },
        {
            'id': 3,
            'bgImg': bgImg3,
            'contentImg': bgContent3,
            'link': ''
        },
        {
            'id': 4,
            'bgImg': bgImg4,
            'contentImg': bgContent4,
            'link': ''
        },
        {
            'id': 5,
            'bgImg': bgImg5,
            'contentImg': bgContent5,
            'link': ''
        },
        {
            'id': 6,
            'bgImg': bgImg6,
            'contentImg': bgContent6,
            'link': ''
        }
    ]
    slider = this.sliders[0];
    state = {
        datas: this.slider,
        currentIndex: 1,
        style1: { 
            backgroundImage: 'url(' + this.slider.bgImg + '?imageView&blur=40x20)',
            textAlign: 'center'},
        style2: {
            backgroundImage: 'url(' + this.slider.contentImg + '?imageView&blur=40x20)',
            textAlign: 'center'
        }
    }
    changeSlider(direction) {
        let currentIndex = this.state.currentIndex;
        let length = this.sliders.length
        if (direction === 1) {
            if (currentIndex < length - 1) {
                this.slider = this.sliders[currentIndex]
                
                this.setState({
                    currentIndex: currentIndex + 1,
                    datas: this.slider
                })
            } else {
                this.slider = this.sliders[0]
                
                this.setState({
                    currentIndex: 1,
                    datas: this.slider
                })
            }
        } else if (direction === 0) {
            if (currentIndex > 1) {
                this.slider = this.sliders[currentIndex - 2]
                
                this.setState({
                    currentIndex: currentIndex - 1,
                    datas: this.slider
                })
            } else {
                this.slider = this.sliders[length - 1]
                
                this.setState({
                    currentIndex: length,
                    datas: this.slider
                })
            }
        }
        this.setState({
            style1: {
                backgroundImage: 'url(' + this.state.datas.bgImg + '?imageView&blur=40x20)',
                textAlign:'center',
                transition:'all 1.0s ease-in-out'
            },
            style2 : {
                backgroundImage: 'url(' + this.state.datas.contentImg + '?imageView&blur=40x20)',
                textAlign:'center',
                transition:'all 1.0s ease-in-out'
            }
        })
    }
    int = setInterval(()=>{
        this.changeSlider(1);
    },1500);
    componentWillUnmount(){
        clearInterval(this.int);
    }

    style3 = {
        backgroundImage: 'url(' + download + '?imageView&blur=40x20)',
    }

    goCover = ()=>{
        this.props.history.push('/discover/coverInfo/7')
    }
    render() {
        return (
            <div className="Slider"  >
                <div className="sld-wrap" style={this.state.style1} onClick={() => { this.goCover() }}>
                    <div className="sld-content">
                        <a className="sld-content-left" style={this.state.style2}></a>
                        <a className="sld-content-right" style={this.style3}></a>
                    </div>
                </div>
                <span className="iconfont icon-xiangzuojiantou" onClick={() => { this.changeSlider(0) }}></span>
                <span className="iconfont icon-xiangyoujiantou" onClick={() => { this.changeSlider(1) }}></span>
            </div>
        )
    }
}

export default withRouter(Slider)
import '../css/Cover.css'
import React, { Component } from 'react'
import headerBGImg from '../../../public/iconall.png'
import { connect } from 'react-redux' //让组件和store连接
import axios from 'axios'
import {withRouter} from 'react-router-dom'

class Cover extends Component {
    coverList = [];
    state = {
        covers : this.coverList
    }
    _path = '../../cover'
    playBtnStyle = {
        background: 'url(' + headerBGImg + ') 0 0 no-repeat'
    }
    ;
    handlerPlayMusic = (songName,songImg,songAllTime) => {
        this.props.playMusic(songName,songImg,songAllTime);
   };

    goCover = (coverId)=>{
        let e = window.event
        if(e.stopPropagation){
             e.stopPropagation()
            }else{
                e.cancaBubble=true
            }
            if(window.event.target.className != "play-btn"){
                this.props.history.push('/discover/coverInfo/'+coverId)
            }
    }
    componentDidMount() {
        axios.get('http://localhost:3000/music163').then((response) => {
            response.data.hotCovers.map((cover) => {
                if (cover.id < 9) {
                    this.coverList.push(cover);
                }
            })
            this.setState({
                covers: this.coverList
            })
            ;
        })
    }
    playSong =  require('../../../public/cover/song/6.m4a');
    coverImg = require('../../../public/cover/image/zj1.jpg')
    render() {
        const coverlist = this.state.covers.map((cover) => {
            return (
                <div key={cover.id} className='cover' onClick={()=>{this.goCover(cover.id)}}>
                    <img src={this._path + '/image/' + cover.img}></img>
                    <a className="cover-info" title={cover.title} href=''>{cover.title}</a>
                    <div className="icon-play">
                        <span className="iconfont icon-18erji-2"></span>
                        <span className="nb">{cover.nb}</span>
                        <span className="play-btn" title="播放" style={this.playBtnStyle} onClick={() => { this.handlerPlayMusic(cover.song,this.coverImg,'2:52') }}></span>
                    </div>
                </div>
            )
        })
        return <div className='CoverList'>
            {coverlist}
        </div>
    }


}
const mapStateToProps = null;

const mapDispatchToProps = (dispatch) => {
    return {
        playMusic: (songName,songImg,songAllTime) => {
            const song = require('../../../public/cover/song/'+songName);
            dispatch({type:'SET_SONG',song:song,songImg:songImg,songAllTime:songAllTime})
    }
}
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Cover))
// export default Cover;
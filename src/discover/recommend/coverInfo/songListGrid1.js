import React, { Component } from 'react'
// import { Table, Tag, Space } from 'antd'
import { connect } from 'react-redux' //让组件和store连接
class SongListGrid extends Component {
    _playImg = 'url(' + require('../../../../public/table.png') + ') 0 -102px no-repeat'
    _playActiveImg = 'url(' + require('../../../../public/table.png') + ') -20px -125px no-repeat'
    state = {
        playSongId: 0
    }
    playInTabletyle = {
        display: 'inline-block',
        width: '20px',
        height: '20px',
        background: this._playImg,
        cursor: 'pointer',
        verticalAlign: 'middle',
        float: 'right',
        marginRight: '10px'
    }
    playInTableActivetyle = {
        display: 'inline-block',
        width: '20px',
        height: '20px',
        background: this._playActiveImg,
        cursor: 'pointer',
        verticalAlign: 'middle',
        float: 'right',
        marginRight: '10px'
    }
    handlerPlayMusic = (songName, songId) => {
        //阻止冒泡
        window.event.cancelBubble = true;
        this.setState({
            playSongId: songId
        })
        this.props.playMusic(songName);
    };
    componentDidUpdate() {
    }
    render() {
        const songList = this.props.songList;
        const tbodyStr = songList ? 
        songList.map((song,index) => {
            return (
                <tr key={song.id} style={index % 2 != 0 ? {backgroundColor: 'rgb(235 235 235)'}:{}}>
                    <td>
                        <div>
                            <span style={{display:'inline-block',height:'20px'}}>{song.id}</span>
                            {(this.state.playSongId != song.id || !this.state.playSongId) ?  <span style={this.playInTabletyle} onClick={() => { this.handlerPlayMusic(song.song,song.id) }}></span> : <span style={this.playInTableActivetyle} onClick={() => { this.handlerPlayMusic(song.song,song.id) }}></span>}
                        </div>
                    </td>
                    <td>
                        <span style={{ cursor: 'pointer' }} onClick={() => { this.handlerPlayMusic(song.song) }}>{song.title}</span>
                    </td>
                    <td>{song.time}</td>
                    <td>{song.singer}</td>
                    <td>{song.album}</td>
                </tr>
            )
        }
        ) 
        : 
        <tr><td colSpan={4}>暂无歌曲</td></tr>

        return (<table border='1' style={{ borderCollapse: 'collapse', borderColor: '#ccc' }}><tr><th></th><th>歌曲标题</th><th>时长</th><th>歌手</th><th>专辑</th></tr>
            {tbodyStr}
        </table>)
    }
}

const mapStateToProps = null;

const mapDispatchToProps = (dispatch) => {
    return {
        playMusic: (songName) => {
            const song = require('../../../../public/songList/' + songName);
            dispatch({ type: 'SET_SONG', song: song,songAllTime:'02:52' })
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(SongListGrid)
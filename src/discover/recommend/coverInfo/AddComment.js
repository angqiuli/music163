import { getDefaultNormalizer } from "@testing-library/react"
import React,{ Component } from "react";
class AddComment extends Component{
    txImg = require('../../../../public/tx.jpg')
    oldVal = ''
    comments = []
    state = {
        comments:this.comments,
        wordsCount:140,
        oldVal:'',
        isOverLength:false
    }
    componentDidMount(){
        this.comments = []
        this.setState({
            comments:this.comments
        })
    }
    commitStyle1 = {
        display:'inline-block',
        // background:'url('+require('../../../../public/button2.png')+'0px -468px no-repeat',
        background: 'url(' + require('../../../../public/button2.png') + ') 0px -468px no-repeat',
        width:'36px',
        height:'31px',
        paddingRight:'8px',
        textAlign: 'right',
        fontSize:'12px',
        color:'#fff',
        lineHeight:'31px',
        verticalAlign:'middle'
    }
    commitStyle2 = {
        display:'inline-block',
        background: 'url(' + require('../../../../public/button2.png') + ')-242px -509px no-repeat',
        height:'31px',
        width:'20px',
        marginLeft:'-3px',
        verticalAlign:'middle'
    }

    checkWords = function(){
        debugger
        let length = window.event.target.value.length
        if(length > 140){
            this.setState({
                isOverLength:true
            })
        }
        this.setState({
            wordsCount:parseInt(140 - length),
            oldVal:window.event.target.value
        })        
    }

    addComment = this.props.addComment
    render(){
        return (
            <div className="AddComment">
                <img className="cmt-tx" src={this.txImg}></img>
                <div className="cnt-right">
                    <div className="input-wrap">
                        <textarea ref="input" rows="3" cols="88" onChange={()=>{this.checkWords()}}></textarea>
                    </div>
                    <div className="jj"></div>
                    <div className="opt-btns">
                        <div className="btns-left"></div>
                        <div className="btns-right">
                            <span className="words-count" style={this.state.isOverLength ? {color:'red'} : {}}>{this.state.wordsCount}</span>
                            <span className="commit">
                                <a className="commit-btn1" style={this.commitStyle1} onClick={()=>{this.addComment(this)}}>提交</a>
                                <a className="commit-btn3" style={this.commitStyle2}></a>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default AddComment
import './Navbar.css'
import React,{Component} from 'react'
import {NavLink} from 'react-router-dom'
import axios from 'axios'

export default class Navbar extends Component {
  state = {
    navbars : [],
    currentbarId:1
  }
  componentWillMount(){
   
  }
  componentDidMount(){
    axios.get("http://localhost:3000/music163").then((response)=>{
      this.setState({
        navbars : response.data.navbars,
        currentNavbarId:1
      })
    })
  }

  isActiveFunc = (navbarId)=>{
      if(navbarId === this.state.currentbarId){
        return true;
      }else{
        return false;
      }
  }
  chooseNavBar = (navbarId)=>{
      this.setState({
        currentNavbarId:navbarId
      })
  }
  render(){
      const navbarList = this.state.navbars.map((navbar)=>{
        return (<li className="module-name" key={navbar.id}><NavLink to={'/'+ navbar.path} className="top-bar-btn" activeClassName='active-bar' isActive={()=>{
          return this.state.currentNavbarId === navbar.id ? true : false;
        }} onClick={()=>this.chooseNavBar(navbar.id)}>{navbar.name}</NavLink>        
       
        </li>        
        )
      })
    return (
      
      <div className="Navbar">
          <div className='bar1-wrapper'>
            <div className="top-bar1">
              <span className='logobg'>
              </span>
              <ul>
                {navbarList}
             </ul> 
              <div className='search-bg'>
                <div className='search-wrap'>
                  <span className='iconfont icon-31sousuo'></span>
                  <input></input>
                </div>
              </div>   
              <div className='clear-content'></div> 
            </div>
          </div>        
      </div>
    );
  }
  
}
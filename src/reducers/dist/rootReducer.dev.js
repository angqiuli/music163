"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
//初始化state
var initState = {
  song: null,
  songAllTime: 0,
  songImg: null,
  playFlag: 0 //0不播放 1播放

};

var rootReducer = function rootReducer() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initState;
  var action = arguments.length > 1 ? arguments[1] : undefined;
  var song = null;
  var songAllTime = 0,
      songImg = null;

  if ('SET_SONG' === action.type) {
    song = action.song;
    songAllTime = action.songAllTime;
    songImg = action.songImg;
    return {
      song: song,
      songAllTime: songAllTime,
      songImg: songImg,
      playFlag: 1
    };
  }

  return state;
};

var _default = rootReducer;
exports["default"] = _default;
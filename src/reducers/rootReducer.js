//初始化state
const initState = {
    song:null,
    songAllTime:0,
    songImg:null,
    playFlag:0 //0不播放 1播放
}
const rootReducer = (state = initState,action)=>{
    let song = null
    let songAllTime = 0,songImg = null
    if('SET_SONG' === action.type){
        song = action.song
        songAllTime = action.songAllTime
        songImg = action.songImg
        return{
            song:song,
            songAllTime: songAllTime,
            songImg:songImg,
            playFlag:1
        }
    }
   return state;
}

export default rootReducer;
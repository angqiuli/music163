import React, { Component } from 'react'
import { Table, Tag, Space } from 'antd'
import { connect } from 'react-redux' //让组件和store连接



class SongListGrid extends Component {
    debugegr;
        playImg = require('../../../../public/table.png');
        playInTableAtyle = {
            display: 'inline-block',
            width: '20px',
            height: '20px',
            background: 'url(' + this.playImg + ') 0 -102px no-repeat',
            cursor:'pointer'

        }
        handlerMouseEnter = () => {
            // 变黑
        }
        handlerMouseOut = () => {
            // 
        }
        // handlerPlayMusic = (songName) => {
        //     //阻止冒泡
        //     window.event.cancelBubble = true;
        //     //let song = require('../../../public/cover/song/'+ songPath);
        //     this.props.playMusic(songName);
        // };
        handlerPlayMusic = () => {
            debugger;
             //阻止冒泡
             window.event.cancelBubble = true;
             let song = require('../../../public/cover/song/'+ songPath);
            this.props.playMusic(song,'','');
       };
        _columns = [];
        _data = [];
        _songList = [];
        
        state = {
            columns:this._columns,
            data :this._date,
            ss:[]
        }
        debugger;
        shouldComponentUpdate(nextProps){ // 应该使用这个方法，否则无论props是否有变化都将会导致组件跟着重新渲染
            if(nextProps.songList === this.props.songList){
              return false
            }
        }
    
        componentDidMount(){
            debugger;
        }
        componentWillReceiveProps(nextProps){
            debugger;
            this._songList = [...this._songList,nextProps.songList];
            this.setState({
                columns:this._columns,
                data :this._data,
                ss:this._songList
            })
            debugger;
        }
        
    render() {            
        this._columns = [
            {
                title: '',
                dataIndex: 'index',
                key: 'index',
                render: (_) => <span className='play-intable' style={this.playInTableAtyle} onMouseEnter={this.handlerMouseEnter} onMouseOut={this.handlerMouseOut}></span>,
            },
            {
                title: '歌曲标题',
                dataIndex: 'title',
                key: 'title',
                render: (_) => <a onMouseEnter={this.handlerMouseEnter} style={{cursor:'pointer'}} onMouseOut={this.handlerMouseOut} onClick={() => this.handlerPlayMusic(_)}>{_}</a>,
            },
            {
                title: '时长',
                dataIndex: 'long',
                key: 'long',
            },
            {
                title: '歌手',
                key: 'singer',
                dataIndex: 'singer'
            }, {
                title: '专辑',
                key: 'album',
                dataIndex: 'album'
            }
        ];
        this._data = this.state.ss ? this.state.ss.map((song) => {
            debugger;
            return {
                index: song.id,
                title: song.song,
                long: song.time,
                singer: song.singer,
                album: song.album
            }
        }) : [];
        
        return (<Table columns={this._columns} dataSource={this._data} />);

    }
}

//export default SongListGrid;


const mapStateToProps = null;

const mapDispatchToProps = (dispatch) => {
    return {
        playMusic: (songName,songImg,songAllTime) => {
            const song = require('../../../public/cover/song/'+songName);
            dispatch({type:'SET_SONG',song:song,songImg:songImg,songAllTime:songAllTime})
    }
}
}
export default connect(mapStateToProps, mapDispatchToProps)(SongListGrid)
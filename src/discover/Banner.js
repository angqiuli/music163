import React ,{Component} from 'react'

export default class Banner extends Component{
    render(){
        return (
            <div className='banner-wrapper'>
                <div className='banner-content'>
                    <div className='poters'></div>
                    <div className='download-content'></div>
                </div>
            </div>
        )
    }
}
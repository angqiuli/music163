import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
// import 'antd/dist/antd.min.css'
import App from './App';
import reportWebVitals from './reportWebVitals';
import { createStore } from 'redux'
import rootReducer from './reducers/rootReducer';
import { Provider } from 'react-redux';//用来连接react和redux


//1、引入redux,react-redux
//2、创建store,需要传入一个reducer,整个reducer要先定义，你需要监听几个状态就可以创建几个
//3、引入provider,并把app包裹，把store传进去，后面的组件才能使用stroe
//创建store(数据存储中心)
const store = createStore(rootReducer)

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  // <React.StrictMode>
  <Provider store={store}>
    <App />
  </Provider>    
  // </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

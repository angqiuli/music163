import React, { ReactDOM, Component } from 'react'
import './CoverInfo.css'
import axios from 'axios'
import icons from '../../../../public/icon.png';
import SongListGrid from './songListGrid1'
import Comment from './Comment'
import AddComment from './AddComment'
import moment from 'moment'
class CoverInfo extends Component {
    constructorI(){
        this.addComment = this.addComment().bind(this)
    }
    that = null;
    _path = '../../../../public/cover';
    _coverInfo = { a: 1, b: 3 };
    _createrImg = null;
    _coverInfo = {
        'id': '',
        'img': '',
        'nb': '',
        'title': '',
        'song': '',
        songList: [],
        createImg: '',
        createName: '',
        createTime: '',
        comments: [],
        instroduce: '',
        commentsLength: 0
    }
    _comments = [];
    state = {
        coverInfo: {},
        showCmts: false,
        comments: this._comments,
        commentsLength: this._comments.length
    }
    componentDidMount() {
        debugger;
        this.that = this
        const coverId = this.props.coverId ? this.props.coverId :this.props.match.params.coverId;
        axios.get('http://localhost:3000/music163').then((response) => {
            this._coverInfo = response.data.hotCovers.find((cover) => {
                return cover.id == coverId;
            })
            this._coverInfo.img = require('../../../../public/cover/image/' + this._coverInfo.img)
            this._coverInfo.createImg = require('../../../../public/cover/creater/' + this._coverInfo.createImg)
            this.setState({
                coverInfo: this._coverInfo
            })
        })

        //查询评论列表
        axios.get('http://localhost:3000/comments').then((response) => {
            this._comments = [...this._comments, ...response.data]
            this.setState({
                comments: this._comments,
                commentsLength: this._comments.length
            })
        })
    }
    showCmts = function () {
        this.setState({
            showCmts: true
        })
       // 点击定位到评论列表
        let int = setInterval(() => {
            if (this.state.showCmts) {
                let anchorElement = this.refs.cmtDom
                anchorElement.scrollIntoView({ behavior: "smooth", block: "start", });
                clearInterval(int);
            }
        }, 200);
    }
    typeStyle = {
        width: '54px',
        height: '24px',
        backgroundPosition: '0 -243px',
        display: 'inline-block',
        verticalAlign: 'top',
        overflow: 'hidden',
        verticalalign: 'middle',
        background: 'url(' + icons + ') 0px -243px no-repeat'
    }

    playBtnStyle = {
        backgroundColor: '#2F7EC9'
    }
    showPlayStyle = {
        background: 'url(' + require('../../../../public/button2.png') + ') 0px -977px no-repeat'
    }
    // 转发
    showShareStyle = {
        background: 'url(' + require('../../../../public/button2.png') + ') 0px -1225px no-repeat'
    }
    showDownloadStyle = {
        background: 'url(' + require('../../../../public/button2.png') + ') 0px -2761px no-repeat'
    }
    showCmtsStyle = {
        background: 'url(' + require('../../../../public/button2.png') + ') 0px -1465px no-repeat'
    }
    btn2Style = {
        background: 'url(' + require('../../../../public/button2.png') + ') -0 9999px no-repeat',
        backgroundPosition: 'right -1020px',
        marginLeft: '-4px',
        width: '5px'

    }

    addComment = function (cmt) {
        debugger;
        let comtObj = {
            "id":Math.random()*1000,
            "cont":cmt.refs.input.value,
            "name":"Hing1017婷",
            "img":"c4.jpg",
            "time": moment().format('YYYY-MM-DD HH:mm:ss'),
            "thumbsUp":0
          }
        if(this.state.isOverLength){
            alert("评价最多140个字！");
        }else{
            axios.post('http://localhost:3000/comments', comtObj).then(()=>{
                this._comments = [comtObj,...this.state.comments]
                        this.setState({
                            comments: this._comments
                        })
                alert("提交成！")
                cmt.refs.input.value = null;

            })
        }        
       
    }
    render() {
        const comments = this.state.comments.map((map) => {
            return (
                <Comment comment={map} />
            )
        })
        return (
            <div className='CoverInfo'>
                <div className='ci-wrap'>
                    <div className='ci-content'>
                        {/* <span className='cover-img' style={this.imgStyle} >
                        </span> */}
                        <img className='cover-img' src={this.state.coverInfo.img}></img>
                        <span className='img-border'></span>
                        <div style={{ float: 'left', marginLeft: '15px', width: '436px' }}>
                            <div className='info-title' >
                                <span className='type-lab' style={this.typeStyle} ></span>
                                <div style={{ marginLeft: '70px', marginTop: '-29px' }}>{this.state.coverInfo.title}</div>
                            </div>
                            <div className='info-creater'>
                                <img className='crtImg' src={this.state.coverInfo.createImg} />
                                <a href=''>{this.state.coverInfo.createName}</a>
                                <span className='crtTime' style={{ lineHeight: '46px' }}>{this.state.coverInfo.createTime}创建</span>
                            </div>
                            <div className='info-bts'>
                                <span className='btn-wrap'><span className='btn' style={this.showPlayStyle}>&nbsp;&nbsp;&nbsp;&nbsp;播放</span><a className='btn' style={this.btn2Style}></a></span>
                                <span className='btn-wrap'><span className='btn' style={this.showPlayStyle}></span><a className='btn' style={this.btn2Style}></a></span>
                                <span className='btn-wrap'><span className='btn' style={this.showShareStyle}></span><a className='btn' style={this.btn2Style}></a></span>
                                <span className='btn-wrap'><span className='btn' style={this.showDownloadStyle}>&nbsp;&nbsp;&nbsp;&nbsp;下载</span><a className='btn' style={this.btn2Style}></a></span>
                                <span className='btn-wrap' onClick={() => this.showCmts()}><span className='btn' style={this.showCmtsStyle}>&nbsp;&nbsp;&nbsp;({this.state.commentsLength})</span><span className='btn' style={this.btn2Style}></span></span>
                            </div>
                            <div className='cover-labs'>
                                标签：
                                <span>华语</span>
                                <span>流行</span>
                                <span>治愈</span>
                            </div>
                            <div className='info-introduce'>
                                介绍：<span>{this.state.coverInfo.introduce}</span>
                            </div>
                        </div>

                        <div className='clearn-content'></div>
                        <div className='songlist-content' >
                            <div className='header'>
                                <a className='header-name'>歌曲列表</a>
                                <span className='counts'>
                                    {/* {this.state.coverInfo.songList.length}首歌 */}
                                </span>
                            </div>
                            {/* 表格 */}
                            <SongListGrid songList={this.state.coverInfo.songList} />

                        </div>
                        {this.state.showCmts ? <div ref="cmtDom" className='commentWrapper' style={{ width: '100%' }}>
                            <div className='header'>
                                <a className='header-name'>评论 &nbsp;<a style={{ fontSize: '12px' }}>(共{this.state.commentsLength}条评论)</a></a>
                            </div>
                            {/* 评论框 */}
                            <AddComment addComment={this.addComment.bind(this) } />
                            {/* 评论列表 */}

                            <div className='comment-header'>
                                <a className='header-name'>最新评论(共{this.state.commentsLength}条评论)</a>
                            </div>
                            {comments}

                        </div> : ''}

                    </div>

                    <div className='info-right'></div>
                </div>
            </div>
        )
    }

}

export default CoverInfo

import React, { Component } from 'react'
import './App.css'
import '../public/font_3435198_wz6m4kt26i8/iconfont.css'
import Navbar from './navbar/Navbar'
import { Route, HashRouter,Switch,Redirect } from 'react-router-dom' //引入路由
// import Home from './home/Home'
import Musician from './musicain/Musician'
import Follow from './follow/Follow'
import Mymusic from './mymusic/Mymusic'
import Shop from './shop/Shop'
import Discover from './discover/Discover'
import SubNav from './SubNav'
// import MyPlayer  from './MyPlayer'
import MyPlayer1 from './MyPlayer1'
class App extends Component {
  render() {
    return (
      <HashRouter>
       
        <div className='App'>
            <Navbar/>
            <div className='content-wrap'>
 {/* 以下的写法是react-router-dom v5的写法 */}
            {/* <SubNav/> */}
            <Switch>
              <Redirect path="/" exact={true} to="/discover/recommend" />  
              <Redirect path="/discover" exact={true} to="/discover/recommend" />  
              <Route path='/discover/:pagename' component={Discover} />
              <Route path='/follow' component={Follow} />
              <Route path='/mymusic' component={Mymusic} />
              <Route path='/musicain' component={Musician} />
              <Route path='/shop' component={Shop} />
            </Switch>
           {/* <Player/>*/}
           
           {/* <MyAudioPlayer/> */}
            </div>
            <MyPlayer1/> 
        </div>

       
      </HashRouter>
    )
  }
}

export default App;
import { Component } from 'react';

export default class Comment extends Component {
    debugger;
    cmtImg = require('../../../../public/cover/comments/' + this.props.comment.img);
    // thumbUpImg = require('../../../../public/icon.png');
    thumbUpStyle = {
        display: 'inline-block',
        width: '16px',
        height: '16px',
        background: 'url(' + require('../../../../public/icon.png') + ') no-repeat -34px -676px'
    }

    thumbUpActiveStyle = {
        display: 'inline-block',
        width: '16px',
        height: '16px',
        background: 'url(' + require('../../../../public/icon.png') + ') no-repeat -34px -714px'
    }
    isThumbUpIds = [];
    state = {
        isThumbUpIds:this.isThumbUpIds,
        thumbUpLength:this.props.comment.thumbsUp
    }
    thumbUp = function (id) {
        debugger;
        // alert("点赞！")
        const res = this.isThumbUpIds.find((item)=>{
            return item === id;
        })
        // 检查id已经存在在数组了，就移除（取消点赞），不存在就添加进去
        if(res){
            this.isThumbUpIds = this.isThumbUpIds.filter((item)=>{
                return item != id;
            })
            //减少评论数
            this.setState({
                thumbUpLength:this.state.thumbUpLength - 1
            })
        }else{
            this.isThumbUpIds.push(id);
            //增加评论数
            this.setState({
                thumbUpLength:this.state.thumbUpLength + 1
            })
        }
        this.setState({
            isThumbUpIds:this.isThumbUpIds
        })
    }
    render() {
        debugger
        return (
            <div className="Comment">
                <div className="cmt-head">
                    <img src={require('../../../../public/cover/comments/' + this.props.comment.img)} />
                </div>
                <div className="cmtWrap">
                    <div className="cmtContent"><a>{this.props.comment.name}:</a>{this.props.comment.cont}</div>
                    <div className="cmtCor">
                        <span className="time">{this.props.comment.time}</span>
                        <div className="btns">
                            <span className="thumbUp-btn" 
                            style={this.state.isThumbUpIds.indexOf(this.props.comment.id) != -1 ? this.thumbUpActiveStyle : this.thumbUpStyle} 
                            onClick={()=>this.thumbUp(this.props.comment.id)}>
                            </span>&nbsp;&nbsp;({this.props.comment.thumbsUp})&nbsp;&nbsp;|&nbsp;&nbsp;
                            <span>回复</span>
                        </div>
                    </div>
                </div>
                <div className="clean-content"></div>
            </div>
        )
    }

}
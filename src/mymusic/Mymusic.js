import React, {  Component } from 'react'
import CoverInfo from '../discover/recommend/coverInfo/CoverInfo'
import '../mymusic/Mymusic.scss'
class Mymusicain extends Component {
  state = {
    showListFlag:1
  }
  jtStyle_hid = {
    borderLeftColor: 'rgb(176, 174, 174)',               
    borderBottomColor: 'transparent',                
    borderTopColor: 'transparent',               
    borderRightColor: 'transparent',     
    verticalAlign: 'middle'   
  }
  jtStyle_show = {
    borderTopColor: 'rgb(176, 174, 174)',               
    borderLeftColor: 'transparent',                
    borderBottomColor: 'transparent',               
    borderRightColor: 'transparent',     
    verticalAlign: 'bottom'  
  }
  showList(){
    this.setState({
      showListFlag:!this.state.showListFlag
    })
  }
  coverImgStyle = {
    width : '25px',
    height : '25px',
    float : 'left',
    margin:'5px 10px'
  }

  render(){
    return (
      <div className="Mymusic">
        <div className="mymusic-wrap">
          <div className="muc-list-wrap">
            <div className='title-wrap' onClick={()=>{this.showList()}}>
              <span className='jt' style={this.state.showListFlag ? this.jtStyle_show : this.jtStyle_hid}></span>
              <span className='title'>我收藏的歌单</span>
            </div>
            <div className='cov-wrap' style={!this.state.showListFlag ? {display:'none'} : {}}>
              <img src={require('../../public/cover/image/zj7.jpg')} style={this.coverImgStyle}></img>
              <div>
                <span className='cover-title'>我对月亮许愿，你要永远快乐</span>
                <span className='cover-li-cu'>11首</span>
              </div>
              
            </div>
          </div>
           <CoverInfo coverId='7'/> 
        </div>
      </div>
    )
  }
  
}

export default Mymusicain;

import { Component } from "react";
import { connect } from 'react-redux' //让组件和store连接

class MyAudioPlayer extends Component{
    
    state = {
        currentTime:0
    }
    setCurrentTime = function(){
        this.setState({
            currentTime:120
        })
        let audioDom = this.refs.audio;
        audioDom.currentTime = 120;
    }
    render(){
        const song = this.props.song;
        if(song){
            return (
                <div className="MyAudioPlayer">
                    <audio ref='audio' src={song} autoPlay={true} controls="true"/>   
                    <button onClick={()=>{this.setCurrentTime()}}>快进到120秒</button>
                    <div className='my-player' style={this.myPlayerStyle}>
                        <div className='play-wrap'>
                            <div className='btns'>
                                <span className='pre' style={this.goPreStyle} onClick={()=>{this.goPre()}}></span>
                                <span className='play' style={this.goPlayStyle} onClick={()=>{this.doPlay()}}></span>
                                <span className='next' style={this.goNextStyle} onClick={()=>{this.goNrxt()}}></span>
                            </div>
                            <div className='progress-wrap'>
                                <img></img>
                                <span className='progress-bar'></span>
                            </div>
                        </div>
                     </div>
                </div>
                            
            )
        }else{
            <div></div>
        }
    }
}

// export default MyAudioPlayer


const mapStateToProps = (state)=>{
    return{
        song:state.song
    }
       
};
export default connect(mapStateToProps)(MyAudioPlayer)
